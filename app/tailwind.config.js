/** @type {import('tailwindcss').Config} */
export default {
    content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
    theme: {
        extend: {
            colors: {
                primary: "#fe696a",
                grayText: "#4D5254",
            },
            fontFamily: {
                body: ["Manrope"],
            },
            spacing: {
                15: "3.75rem",
            },
        },
        container: {
            padding: {
                DEFAULT: "1.25rem",
                sm: "0",
                lg: "0",
                xl: "0",
                "2xl": "0",
            },
        },
        screens: {
            sm: "640px",
            md: "768px",
            lg: "992px", //1245px
            xl: "1245px", //1280px
            "2xl": "1536px",
        },
    },
    plugins: [],
};
