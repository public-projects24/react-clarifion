import StepperBar from "@/components/common/StepperBar";
import TitleSection from "@/components/common/TitleSection";
import ProductDetail from "@/components/product/ProductDetail";

export default function HomePage() {
    return (
        <>
            <section className="section container mx-auto">
                <TitleSection
                    title="Wait ! your order in progress."
                    subtitle="Lorem ipsum dolor sit amet, consectetur adipiscing"
                />
                <StepperBar />
                <ProductDetail />
            </section>
        </>
    );
}
