import { Outlet } from "react-router-dom";
import NavMenu from "@/components/common/NavMenu";
import { Suspense } from "react";
import UpperMenu from "@/components/common/UpperMenu";
import Footer from "@/components/common/Footer";

function Default() {
    return (
        <>
            <UpperMenu />
            <NavMenu />
            <div id="main-content">
                <Suspense fallback={<h1>Loading ...</h1>}>
                    <Outlet />
                </Suspense>
            </div>
            <Footer />
        </>
    );
}

export default Default;
