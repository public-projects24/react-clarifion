import { createBrowserRouter } from "react-router-dom";
import Default from "@/layouts/Default";
import { lazy } from "react";

const ErrorPage = lazy(() => import("@/pages/ErrorPage"));
const HomePage = lazy(() => import("@/pages/HomePage"));
const ProductsPage = lazy(() => import("@/pages/ProductsPage"));

export const router = createBrowserRouter([
    {
        path: "/",
        element: <Default />,
        errorElement: <ErrorPage />,
        children: [
            {
                // Forma 1: Lazy
                //async lazy() {
                //    let { HomePage } = await import("@/pages/HomePage");
                //    return { Component: HomePage };
                //},
                path: "/",
                element: <HomePage />,
            },
            {
                path: "products",
                element: <ProductsPage />,
            },
        ],
    },
]);

/* export const router = createBrowserRouter(
    createRoutesFromElements(
        <Route path="/" element={<Default />} errorElement={<ErrorPage />}>
            <Route path="/" element={<HomePage />} />
            <Route path="products" element={<ProductsPage />} />
        </Route>
    )
);
 */
