import axios from "axios";

let api = axios.create({
    baseURL: import.meta.env.VITE_API_ENDPOINT,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
    },
});

api.interceptors.response.use(
    function (response) {
        // Any status code that lie within the range of 2xx cause this function to trigger
        // Do something with response data
        return response;
    },
    function (error) {
        // Any status codes that falls outside the range of 2xx cause this function to trigger
        // Do something with response error
        interceptorOnError(error);
        return Promise.reject(error);
    }
);

async function interceptorOnError(error: any) {
    console.log("error interceptor ", error.response);
    //console.log("the route is: ", router.currentRoute);

    // If user is unauthenticated
    if (error.response.status === 401) {
        console.log("error status 401");
    }
}
export default api;
