declare global {
    type Product = {
        category: object;
        creationAt: string;
        description: string;
        id: number;
        images: string[];
        price: number;
        title: string;
        updatedAt: string;
    };
}

export {};
