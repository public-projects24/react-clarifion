import "@/assets/css/components/product/product-detail.css";
import ProductInfo from "@/components/product/ProductInfo";
import ProductPreview from "@/components/product/ProductPreview";
import ProductFeatures from "@/components/product/ProductFeatures";
import ProductSavings from "./ProductSavings";
import ProductButton from "./ProductButton";
import ProductShipping from "./ProductShipping";
import ProductGuarantee from "./ProductGuarantee";
import ProductComments from "./ProductComments";

export default function ProductDetail() {
    return (
        <div className="product-detail">
            <h2 className="product-detail__title-mobile">
                <span className="product-detail__title-mobile--accent">
                    ONE TIME ONLY
                </span>{" "}
                special price for 6 extra Clarifion for only
                <span className="product-detail__title-mobile--accent">
                    $14 each
                </span>{" "}
                ($84.00 total!)
            </h2>
            {/* <div className=" grid md:grid-cols-2 md:gap-10"> */}
            <div className="product-detail__container">
                <div className="product-detail__image">
                    <img src="/img/product-detail/product.png" alt="product" />
                    <ProductComments />
                </div>
                <div>
                    <ProductInfo />
                    <ProductPreview />
                    <ProductFeatures />
                    <ProductSavings />
                    <ProductButton />
                    <ProductShipping />
                    <ProductGuarantee />
                </div>
            </div>
        </div>
    );
}
