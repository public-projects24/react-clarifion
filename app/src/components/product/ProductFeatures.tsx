import "@/assets/css/components/product/product-features.css";

export default function ProductFeatures() {
    return (
        <div className="product-features">
            <div className="product-features__feature">
                <img
                    src="/img/product-detail/tick-circle.svg"
                    alt="tick"
                    className="product-features__icon"
                />
                <div className="product-features__description">
                    Negative Ion Technology may{" "}
                    <span className="product-features__description--accent">
                        help with allergens
                    </span>
                </div>
            </div>
            <div className="product-features__feature">
                <img
                    src="/img/product-detail/tick-circle.svg"
                    alt="tick"
                    className="product-features__icon"
                />
                <div className="product-features__description">
                    Designed for{" "}
                    <span className="product-features__description--accent">
                        air rejuvenation
                    </span>
                </div>
            </div>
            <div className="product-features__feature">
                <img
                    src="/img/product-detail/tick-circle.svg"
                    alt="tick"
                    className="product-features__icon"
                />
                <div className="product-features__description">
                    <span className="product-features__description--accent">
                        Perfect for every room
                    </span>{" "}
                    in all types of places
                </div>
            </div>
        </div>
    );
}
