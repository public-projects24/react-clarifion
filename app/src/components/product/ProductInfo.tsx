import "@/assets/css/components/product/product-info.css";

export default function ProductInfo() {
    return (
        <div className="product-info">
            <h2 className="product-info__title">
                <span className="product-info__title--accent">
                    ONE TIME ONLY
                </span>{" "}
                special price for 6 extra Clarifion for only
                <span className="product-info__title--accent">
                    $14 each
                </span>{" "}
                ($84.00 total!)
            </h2>
        </div>
    );
}
