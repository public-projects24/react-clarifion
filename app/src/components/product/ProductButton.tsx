import "@/assets/css/components/product/product-button.css";

export default function ProductButton() {
    return (
        <button className="btn btn--primary product-button">
            Yes - Claim my discount
            <img
                src="/img/common/button/right-arrow.svg"
                alt="icon"
                className="btn__icon"
            />
        </button>
    );
}
