import "@/assets/css/components/common/product-card.css";

type ProductCardProps = {
    product: Product;
};

const ProductCard = ({ product }: ProductCardProps) => {
    return (
        <div className="product-card">
            <a href="#" className="product-card__link">
                <img src={product.images[0]} alt="product" />
                <h3 className="product-card__title">{product.title}</h3>
                <p className="product-card__price">${product.price}</p>
            </a>
        </div>
    );
};

export default ProductCard;
