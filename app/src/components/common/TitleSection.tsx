import "@/assets/css/components/common/title-section.css";

type TitleSectionProps = {
    title: string;
    subtitle: string;
};
const TitleSection = (props: TitleSectionProps) => {
    return (
        <div className="title-section">
            <h2 className="title-section__title">{props.title}</h2>
            <h2 className="title-section__subtitle">{props.subtitle}</h2>
        </div>
    );
};

export default TitleSection;
