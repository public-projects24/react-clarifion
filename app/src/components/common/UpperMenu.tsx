import "@/assets/css/components/common/uppermenu.css";

export default function UpperMenu() {
    return (
        <div className="uppermenu">
            <div className="uppermenu__container container mx-auto">
                <div className="uppermenu__arrow">
                    <img
                        src="/img/common/uppermenu/arrow-left.svg"
                        alt="left arrow"
                    />
                </div>
                <div className="uppermenu__items">
                    <div className="uppermenu__item uppermenu__item--active">
                        <img
                            src="/img/common/uppermenu/chek.svg"
                            className="uppermenu__icon"
                            alt="check"
                        />
                        30-DAY SATISFACTION GUARANTEE
                    </div>
                    <div className="uppermenu__item">
                        <img
                            src="/img/common/uppermenu/truck.svg"
                            className="uppermenu__icon"
                            alt="fre delivery"
                        />
                        Free delivery on orders over $40.00
                    </div>
                    <div className="uppermenu__item">
                        <img
                            src="/img/common/uppermenu/heart.svg"
                            className="uppermenu__icon"
                            alt="happy customers"
                        />
                        50.000+ HAPPY CUSTOMERS
                    </div>
                </div>
                <div className="uppermenu__arrow">
                    <img
                        src="/img/common/uppermenu/arrow-right.svg"
                        alt="right arrow"
                    />
                </div>
            </div>
        </div>
    );
}
