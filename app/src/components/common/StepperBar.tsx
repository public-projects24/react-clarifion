import "@/assets/css/components/common/stepper-bar.css";

export default function StepperBar() {
    return (
        <div className="stepper">
            <div className="stepper__items">
                <div className="stepper__item">
                    <div className="stepper__circle stepper__circle--checked">
                        <img
                            src="/img/common/stepper/check-mark.svg"
                            alt="checked"
                            className="stepper__icon"
                        />
                    </div>
                    <span className="stepper__item-number">Step 1: </span>
                    <span className="stepper__item-title">Cart Review</span>
                </div>
                <div className="stepper__item">
                    <div className="stepper__circle stepper__circle--checked">
                        <img
                            src="/img/common/stepper/check-mark.svg"
                            alt="checked"
                            className="stepper__icon"
                        />
                    </div>
                    <span className="stepper__item-number">Step 2: </span>
                    <span className="stepper__item-title">Checkout</span>
                </div>
                <div className="stepper__item stepper__item--current">
                    <div className="stepper__circle stepper__circle--current">
                        3
                    </div>
                    <span className="stepper__item-number">Step 3: </span>
                    <span className="stepper__item-title">Special Offer</span>
                </div>
                <div className="stepper__item ">
                    <div className="stepper__circle stepper__circle--incompleted">
                        4
                    </div>
                    <span className="stepper__item-number">Step 4: </span>
                    <span className="stepper__item-title">Confirmation</span>
                </div>
            </div>
        </div>
    );
}
