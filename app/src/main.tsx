import React from "react";
import ReactDOM from "react-dom/client";
import { RouterProvider } from "react-router-dom";
import { router } from "./routes/index";

import { store } from "@/store/store";
import { Provider } from "react-redux";

import "./assets/css/app.css";
//import App from "./App.tsx";

ReactDOM.createRoot(document.getElementById("root")!).render(
    <React.StrictMode>
        <Provider store={store}>
            <RouterProvider router={router} />
            {/* <App /> */}
        </Provider>
    </React.StrictMode>
);
